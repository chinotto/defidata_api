const Pool = require('pg').Pool

//ADD YOUR SERVER
const pool = new Pool({
  user: '{USER}',
  host: '{HOST}',
  database: '{DB}',
  password: '{PASSWORD',
  port: '{PORT}',
})


const tweet_trend = (request, response) => {
  now_floor  = parseInt( (Date.now()/1000) - 7890000);
    pool.query("SELECT date, sum(count) AS tweets FROM rsd_metrics WHERE symbol != 'NOT_A_SYMBOL' AND timestamp >= " + now_floor + "GROUP BY 1", (error, results)  => {
      if (error) {
        throw error
      }
      response.setHeader('Content-Type', 'application/json;charset=UTF-8');
      response.setHeader('Cache-Control', 'max-age=60, stale-while-revalidate');
      response.status(200).json(results.rows)
    })
  }

const history = (request, response) => {
  now_floor  = parseInt( (Date.now()/1000) - 7890000);
    pool.query("SELECT * FROM rsd_metrics WHERE symbol != 'NOT_A_SYMBOL' AND LOWER(symbol) = LOWER('" +request.params.symbol +"') AND timestamp >= " + now_floor, (error, results)  => {
      if (error) {
        throw error
      }
      response.setHeader('Content-Type', 'application/json;charset=UTF-8');
      response.setHeader('Cache-Control', 'max-age=60, stale-while-revalidate');
      response.status(200).json(results.rows)
    })
  }

  const trending = (request, response) => {
    pool.query(`SELECT * FROM rsd_metrics WHERE symbol != 'NOT_A_SYMBOL' AND timestamp >= (SELECT MAX(timestamp) FROM rsd_metrics) AND count >= 100 ORDER BY sma1_dif DESC LIMIT 10`, (error, results) => {
      if (error) {
        throw error
      }
      response.setHeader('Content-Type', 'application/json;charset=UTF-8');
      response.setHeader('Cache-Control', 'max-age=60, stale-while-revalidate');
      response.status(200).json(results.rows)
    })
  }

  const volatile = (request, response) => {
    pool.query(`SELECT * FROM rsd_metrics WHERE symbol != 'NOT_A_SYMBOL' AND timestamp >= (SELECT MAX(timestamp) FROM rsd_metrics) AND count >= 100 ORDER BY rsd_7 DESC LIMIT 10`, (error, results) => {
      if (error) {
        throw error
      }
      response.setHeader('Content-Type', 'application/json;charset=UTF-8');
      response.setHeader('Cache-Control', 'max-age=60, stale-while-revalidate');
      response.status(200).json(results.rows)
    })
  }

  const descreasing = (request, response) => {
    pool.query(`SELECT * FROM rsd_metrics WHERE symbol != 'NOT_A_SYMBOL' AND timestamp >= (SELECT MAX(timestamp) FROM rsd_metrics) AND count >= 100 ORDER BY sma1_dif ASC LIMIT 10`, (error, results) => {
      if (error) {
        throw error
      }
      response.setHeader('Content-Type', 'application/json;charset=UTF-8');
      response.setHeader('Cache-Control', 'max-age=60, stale-while-revalidate');
      response.status(200).json(results.rows)
    })
  }



  const tokenstats = (request, response) => {
    pool.query("SELECT * FROM rsd_metrics WHERE symbol != 'NOT_A_SYMBOL' AND timestamp >= (SELECT MAX(timestamp) FROM rsd_metrics) AND LOWER(symbol) = LOWER('" +request.params.symbol +"') LIMIT 1", (error, results) => {
      if (error) {
        throw error
      }
      response.setHeader('Content-Type', 'application/json;charset=UTF-8');
      response.setHeader('Cache-Control', 'max-age=60, stale-while-revalidate');
      response.status(200).json(results.rows)
    })
  }


  const tokens = (request, response) => {
    pool.query(`SELECT * FROM tokens`, (error, results) => {
      if (error) {
        throw error
      }
      response.setHeader('Content-Type', 'application/json;charset=UTF-8');
      response.setHeader('Cache-Control', 'max-age=60, stale-while-revalidate');
      response.status(200).json(results.rows)
    })
  }


  module.exports = {
    tweet_trend,
    history,
    trending,
    volatile,
    descreasing,
    tokenstats,
    tokens,
  }

