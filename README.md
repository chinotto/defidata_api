# Simple Express JS API Server

A very simple low footprint Express Js server for injecting data from Postgres DB into the website [DefiData](https://www.defidata.dev/)

## JAMSTACK

DefiData is a [Jamstack](https://jamstack.org/) application. Therefore we serve pages as static HTML and inject dynamic data via API.  

