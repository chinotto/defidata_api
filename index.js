const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = process.env.PORT
const db = require('./queries')
var cors = require("cors")


app.use(bodyParser.json())
app.use(cors())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)


app.get('/', (request, response) => {
    response.setHeader('Content-Type', 'application/json;charset=UTF-8');
    response.setHeader('Cache-Control', 's-max-age=1, stale-while-revalidate');
    response.json({ info: 'Defi Data API endpoint' })
  })

app.get('/alltweets', db.tweet_trend)
app.get('/trending', db.trending)
app.get('/volatile', db.volatile)
app.get('/decreasing', db.descreasing)
app.get('/tokens', db.tokens)
app.get('/tokenstats/:symbol', db.tokenstats)
app.get('/history/:symbol', db.history)


app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})

module.exports = app
